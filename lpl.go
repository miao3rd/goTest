package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/rand"
	"sort"
	"sync"
	"time"
)

const TEST_NUM = 1000000

type LPLTeam struct {
	Name  string
	ELO   int
	Level int

	Win      int
	Los      int
	ScoreWin int
	ScoreLos int

	Champion int
	PlayOff  int
	Worlds   int
}

type GameLog struct {
	Teams [2]string
	Score [2]int
}

type LPLCfg struct {
	Teams   []LPLTeam
	Logs    []GameLog
	PlayOff []GameLog
	SLevel  [8]int
}

func (log *GameLog) Result() (string, string) {
	if log.Score[0] == 3 {
		return log.Teams[0], log.Teams[1]
	}
	if log.Score[1] == 3 {
		return log.Teams[1], log.Teams[0]
	}
	if log.Score[0] == 2 {
		return log.Teams[0], log.Teams[1]
	}
	if log.Score[1] == 2 {
		return log.Teams[1], log.Teams[0]
	}
	return "", ""
}

func (cfg *LPLCfg) QueryTeam(name string) *LPLTeam {
	for k, v := range cfg.Teams {
		if v.Name == name {
			return &cfg.Teams[k]
		}
	}
	return nil
}

func (cfg *LPLCfg) SetScore(log *GameLog, e0 float64) {
	w, l := log.Result()

	wt := cfg.QueryTeam(w)
	wt.Win++

	lt := cfg.QueryTeam(l)
	lt.Los++

	e1 := float64(1) - e0
	if log.Score[0] == 2 {
		cfg.QueryTeam(log.Teams[0]).ELO += int(float64(ELO_VAL) * (float64(1) - e0))
		cfg.QueryTeam(log.Teams[1]).ELO += int(float64(ELO_VAL) * (float64(0) - e1))
	} else if log.Score[1] == 2 {
		cfg.QueryTeam(log.Teams[0]).ELO += int(float64(ELO_VAL) * (float64(0) - e0))
		cfg.QueryTeam(log.Teams[1]).ELO += int(float64(ELO_VAL) * (float64(1) - e1))
	}

	if w == log.Teams[0] {
		wt.ScoreWin += log.Score[0]
		wt.ScoreLos += log.Score[1]

		lt.ScoreWin += log.Score[1]
		lt.ScoreLos += log.Score[0]
	} else {
		wt.ScoreWin += log.Score[1]
		wt.ScoreLos += log.Score[0]

		lt.ScoreWin += log.Score[0]
		lt.ScoreLos += log.Score[1]
	}
}

func (cfg *LPLCfg) Result(a, b string) bool {
	for _, v := range cfg.Logs {
		if v.Teams[0] != a && v.Teams[1] != a {
			continue
		}
		if v.Teams[0] != b && v.Teams[1] != b {
			continue
		}
		if v.Teams[0] == a && v.Score[0] == 2 {
			return true
		}
		if v.Teams[1] == a && v.Score[1] == 2 {
			return true
		}
		break
	}
	return false
}

func simulate(cfg *LPLCfg, wg *sync.WaitGroup) {
	var e Elo
	e.Setup(*cfg)
	e.Season(false)

	var team *LPLTeam
	for _, v := range e.Ranks {
		team = cfg.QueryTeam(v)
		team.PlayOff++
	}

	e.Playoffs()
	cfg.QueryTeam(e.Cfg.Teams[0].Name).Champion++

	e.Ticks()
	for _, v := range e.Worlds {
		cfg.QueryTeam(v).Worlds++
	}

	wg.Done()
}

func DoLPL(path string) error {
	var cfg LPLCfg

	data, err := ioutil.ReadFile("./lpl/" + path + ".json")
	if err != nil {
		return err
	}

	err = json.Unmarshal(data, &cfg)
	if err != nil {
		return err
	}

	var eloBase Elo
	eloBase.Setup(cfg)
	lastIdx := eloBase.Season(true)
	totalIdx := len(cfg.Logs)
	if lastIdx == -1 {
		lastIdx = totalIdx + len(cfg.PlayOff)
	}

	fmt.Printf("last=%d total=%d\n", lastIdx, totalIdx)
	fmt.Println("---------")

	for k, v := range eloBase.Cfg.Teams {
		fmt.Printf("rank[%d] = %+v\n", k+1, v)
	}
	fmt.Println("---------")

	rand.Seed(time.Now().UnixNano())

	wg := sync.WaitGroup{}
	wg.Add(TEST_NUM)

	for i := 0; i < TEST_NUM; i++ {
		go simulate(&cfg, &wg)
	}
	wg.Wait()

	sort.Slice(cfg.Teams, func(i, j int) bool {
		vi := cfg.Teams[i].Champion + cfg.Teams[i].PlayOff + cfg.Teams[i].Worlds
		vi = vi*1000 + cfg.Teams[i].ELO
		vj := cfg.Teams[j].Champion + cfg.Teams[j].PlayOff + cfg.Teams[j].Worlds
		vj = vj*1000 + cfg.Teams[j].ELO
		return vi > vj
	})

	var lplDB LplElo
	lplDB.Idx = lastIdx

	for k, v := range cfg.Teams {
		crate := float64(v.Worlds*100) / float64(TEST_NUM)
		prate := float64(0)
		if lastIdx < totalIdx {
			prate = float64(v.PlayOff*100) / float64(TEST_NUM)
		} else {
			prate = float64(v.Champion*100) / float64(TEST_NUM)
		}

		if crate == 0 && prate == 0 {
			fmt.Printf("rank[%d] = %s -- --\n", k+1, v.Name)
		} else if crate == 0 {
			fmt.Printf("rank[%d] = %s %.2f%% --\n", k+1, v.Name, prate)
		} else if prate == 0 {
			fmt.Printf("rank[%d] = %s -- %.2f%%\n", k+1, v.Name, crate)
		} else {
			fmt.Printf("rank[%d] = %s %.2f%% %.2f%%\n", k+1, v.Name, prate, crate)
		}

		lplDB.ID = uint(lplDB.Idx*100 + k)
		lplDB.Team = v
		result := g_mailDB.Model(&LplElo{}).Create(&lplDB)
		if result == nil {
			fmt.Printf("%+v can not found insert to db", lplDB)
		}
	}
	fmt.Println("---------")

	return nil
}
