package main

import (
	"fmt"
	"strconv"
)

func DoSkillCalc(addStr string) error {
	var points = []int{
		3, 4, 5, 7, 8, 9, 11, 13, 14, 16,
		18, 19, 21, 23, 25, 27, 29, 32, 34, 36,
		38, 41, 43, 46, 48, 51, 54, 56, 59, 62,
		65, 68, 71, 74, 77, 81, 84, 87, 91, 94,
		98, 101, 105, 108, 112, 116, 120, 124, 128, 132}
	l := len(points)

	add, err := strconv.Atoi(addStr)
	if err != nil {
		return err
	}

	var offset []int
	if add == 0 {
		for k, v := range points {
			if k+1 < l {
				offset = append(offset, points[k+1]-v)
			}
		}
	} else {
		var sum int
		for k, v := range points {
			if k%add == 0 {
				sum = 0
			}
			sum += v
			if k%add == add-1 {
				offset = append(offset, sum)
			}
		}
	}

	fmt.Printf("offset = %+v\n", offset)

	return nil
}
