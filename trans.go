package main

import (
	"context"
	"fmt"

	trans "cloud.google.com/go/translate"
	translate "cloud.google.com/go/translate/apiv3"
	"golang.org/x/text/language"
	translatepb "google.golang.org/genproto/googleapis/cloud/translate/v3"
)

// export GOOGLE_APPLICATION_CREDENTIALS="/Users/dxl/work/goTest/asdxl198618-9f8873c64f0b.json"

func translateText(projectID string, sourceLang string, targetLang string, text string) error {
	ctx := context.Background()
	client, err := translate.NewTranslationClient(ctx)
	if err != nil {
		return fmt.Errorf("NewTranslationClient: %v", err)
	}
	defer client.Close()

	req := &translatepb.TranslateTextRequest{
		Parent:             fmt.Sprintf("projects/%s/locations/global", projectID),
		SourceLanguageCode: sourceLang,
		TargetLanguageCode: targetLang,
		MimeType:           "text/plain", // Mime types: "text/plain", "text/html"
		Contents:           []string{text},
	}

	resp, err := client.TranslateText(ctx, req)
	if err != nil {
		return fmt.Errorf("TranslateText: %v", err)
	}

	// Display the translation for each input text provided
	for _, translation := range resp.GetTranslations() {
		fmt.Printf("Translated text: %v\n", translation.GetTranslatedText())
	}

	return nil
}

func detectLanguage(text string) (*trans.Detection, error) {
	ctx := context.Background()
	client, err := trans.NewClient(ctx)
	if err != nil {
		return nil, err
	}
	defer client.Close()

	lang, err := client.DetectLanguage(ctx, []string{text})
	if err != nil {
		return nil, err
	}
	return &lang[0][0], nil
}

func transLanguage(text string) (string, error) {
	ctx := context.Background()

	lang, err := language.Parse("zh-CN")
	if err != nil {
		return "", fmt.Errorf("language.Parse: %v", err)
	}

	client, err := trans.NewClient(ctx)
	if err != nil {
		return "", err
	}
	defer client.Close()

	resp, err := client.Translate(ctx, []string{text}, lang, nil)
	if err != nil {
		return "", fmt.Errorf("Translate: %v", err)
	}
	if len(resp) == 0 {
		return "", fmt.Errorf("Translate returned empty response to text: %s", text)
	}
	return resp[0].Text, nil
}
