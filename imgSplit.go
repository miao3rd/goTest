package main

import (
	"fmt"
	"image"
	"image/png"
	"io/ioutil"
	"os"
	FP "path"

	"github.com/tidwall/gjson"
)

func DoImgSplit(filename string) error {
	path := fmt.Sprintf("./texturePacker/%s.png", filename)
	file, err := os.Open(path)
	if err != nil {
		return err
	}

	img, err := png.Decode(file)
	if err != nil {
		return err
	}
	size := img.Bounds().Size()
	fmt.Printf("read %s size:%+v\n", path, size)

	path = fmt.Sprintf("./texturePacker/%s.json", filename)
	json, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}

	_, err = os.Stat("./out")
	if err != nil && os.IsNotExist(err) {
		err = os.Mkdir("./out", 0777)
		if err != nil {
			return err
		}
	}

	frames := gjson.GetBytes(json, `frames`)
	frames.ForEach(func(key, value gjson.Result) bool {
		filePath := "./out/" + key.String()
		fileType := FP.Ext(FP.Base(filePath))
		if len(fileType) == 0 {
			filePath = filePath + ".png"
		}

		newImg, err := os.Create(filePath)
		if err != nil {
			fmt.Println(err.Error())
			return false
		}
		defer newImg.Close()

		w := value.Get("sourceSize.w").Int()
		h := value.Get("sourceSize.h").Int()
		rgba := image.NewRGBA(image.Rect(0, 0, int(w), int(h)))

		sx := value.Get("spriteSourceSize.x").Int()
		sy := value.Get("spriteSourceSize.y").Int()
		sw := value.Get("spriteSourceSize.w").Int()
		sh := value.Get("spriteSourceSize.h").Int()
		dx := value.Get("frame.x").Int()
		dy := value.Get("frame.y").Int()

		for x := int64(0); x < sw; x++ {
			for y := int64(0); y < sh; y++ {
				rgba.Set(int(x+sx), int(y+sy), img.At(int(x+dx), int(y+dy)))
			}
		}

		err = png.Encode(newImg, rgba)
		if err != nil {
			panic(err.Error())
		}

		fmt.Printf("complete - %s\n", filePath)

		return true
	})

	return nil
}
