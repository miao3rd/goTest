package main

import (
	"math"
	"math/rand"
	"sort"
)

type Elo struct {
	Cfg    LPLCfg
	Ranks  []string
	Worlds [4]string
}

const ELO_VAL = 25

func (e *Elo) Setup(cfg LPLCfg) {
	for k, _ := range cfg.Teams {
		e.Cfg.Teams = append(e.Cfg.Teams, cfg.Teams[k])
	}
	for k, _ := range cfg.Logs {
		e.Cfg.Logs = append(e.Cfg.Logs, cfg.Logs[k])
	}
	for k, _ := range cfg.PlayOff {
		e.Cfg.PlayOff = append(e.Cfg.PlayOff, cfg.PlayOff[k])
	}
	for k, _ := range cfg.SLevel {
		e.Cfg.SLevel[k] = cfg.SLevel[k]
	}
}

func (e *Elo) QueryTeam(name string) *LPLTeam {
	return e.Cfg.QueryTeam(name)
}

func (e *Elo) Rank() {
	sort.Slice(e.Cfg.Teams, func(i, j int) bool {
		// 大分
		si := e.Cfg.Teams[i].Win - e.Cfg.Teams[i].Los
		sj := e.Cfg.Teams[j].Win - e.Cfg.Teams[j].Los
		if si != sj {
			return si > sj
		}

		// 小分
		si = e.Cfg.Teams[i].ScoreWin - e.Cfg.Teams[i].ScoreLos
		sj = e.Cfg.Teams[j].ScoreWin - e.Cfg.Teams[j].ScoreLos
		if si != sj {
			return si > sj
		}

		// 胜负关系
		return e.Cfg.Result(e.Cfg.Teams[i].Name, e.Cfg.Teams[j].Name)
	})
}

/*
Ra: A选手当前分数
Rb: B选手当前分数

Ea: 预期A选手的胜负值
Ea = 1/(1+10^[(Rb-Ra)/400])

Eb: 预期B选手的胜负值
Eb = 1/(1+10^[(Ra-Rb)/400])

E值也是预估的双方胜率，所以 Ea + Eb = 1

Sa: 实际胜负值，胜 = 1， 平 = 0.5， 负 = 0
K： 每场比赛能得到的最大分数，魔兽里 k=32

R'a: A选手一场比赛之后的积分
R'a = Ra + K(Sa-Ea)

R'b: B选手一场比赛之后的积分
R'b = Rb + K(Sa-Eb)
*/

func EloCalc(a, b int) float64 {
	e0 := float64(b-a) / float64(400)
	return float64(1) / (math.Pow(10, e0) + float64(1))
}

func (e *Elo) PlayBO3(log *GameLog) {
	t0 := e.QueryTeam(log.Teams[0])
	t1 := e.QueryTeam(log.Teams[1])
	e0 := EloCalc(t0.ELO, t1.ELO)

	if log.Score[0]+log.Score[1] == 0 {
		for i := 0; i < 3; i++ {
			if rand.Float64() < e0 {
				log.Score[0]++
			} else {
				log.Score[1]++
			}
		}
	}

	e.Cfg.SetScore(log, e0)
}

func (e *Elo) Season(now bool) int {
	lastIdx := -1
	for k, v := range e.Cfg.Logs {
		if v.Score[0]+v.Score[1] == 0 && now {
			lastIdx = k
			break
		}
		e.PlayBO3(&e.Cfg.Logs[k])
	}

	e.Rank()

	if !now {
		for k, v := range e.Cfg.Teams {
			if k == 10 {
				break
			}
			e.Ranks = append(e.Ranks, v.Name)
		}
	}

	return lastIdx
}

func (e *Elo) PlayBO5(log *GameLog) (string, string) {
	if log.Score[0]+log.Score[1] == 0 {
		t0 := e.QueryTeam(log.Teams[0])
		t1 := e.QueryTeam(log.Teams[1])
		e0 := EloCalc(t0.ELO, t1.ELO)

		for i := 0; i < 5; i++ {
			if rand.Float64() < e0 {
				log.Score[0]++
			} else {
				log.Score[1]++
			}
			if log.Score[0] == 3 || log.Score[1] == 3 {
				break
			}
		}
	}
	return log.Result()
}

func (e *Elo) Playoffs() {
	var log GameLog
	// 9-8-5-4-1
	// 10-7-6-3-2
	if len(e.Cfg.PlayOff) == 0 {
		log.Teams[0] = e.Ranks[8]
		log.Teams[1] = e.Ranks[7]
		e.Cfg.PlayOff = append(e.Cfg.PlayOff, log)
	}

	if len(e.Cfg.PlayOff) == 1 {
		log.Teams[0] = e.Ranks[9]
		log.Teams[1] = e.Ranks[6]
		e.Cfg.PlayOff = append(e.Cfg.PlayOff, log)
	}

	var idxs = [6]int{4, 5, 3, 2, 0, 1}
	var level = [6]int{-1, -1, 7, 6, 5, 4}
	for i := 0; i < 6; i++ {
		w, l := e.PlayBO5(&e.Cfg.PlayOff[i])
		if i+2 == len(e.Cfg.PlayOff) {
			log.Teams[0] = e.Ranks[idxs[i]]
			log.Teams[1] = w
			e.Cfg.PlayOff = append(e.Cfg.PlayOff, log)

			if level[i] > 0 {
				e.QueryTeam(l).Level += e.Cfg.SLevel[level[i]]
			}
		}
	}

	w1, l1 := e.PlayBO5(&e.Cfg.PlayOff[6])
	w2, l2 := e.PlayBO5(&e.Cfg.PlayOff[7])
	if len(e.Cfg.PlayOff) == 8 {
		log.Teams[0] = l1
		log.Teams[1] = l2
		e.Cfg.PlayOff = append(e.Cfg.PlayOff, log)
	}
	if len(e.Cfg.PlayOff) == 9 {
		log.Teams[0] = w1
		log.Teams[1] = w2
		e.Cfg.PlayOff = append(e.Cfg.PlayOff, log)
	}

	w1, l1 = e.PlayBO5(&e.Cfg.PlayOff[8])
	e.QueryTeam(l1).Level += e.Cfg.SLevel[3]

	w2, l2 = e.PlayBO5(&e.Cfg.PlayOff[9])

	if len(e.Cfg.PlayOff) == 10 {
		log.Teams[0] = w1
		log.Teams[1] = l2
		e.Cfg.PlayOff = append(e.Cfg.PlayOff, log)
	}

	w1, l1 = e.PlayBO5(&e.Cfg.PlayOff[10])
	e.QueryTeam(l1).Level += e.Cfg.SLevel[2]

	if len(e.Cfg.PlayOff) == 11 {
		log.Teams[0] = w2
		log.Teams[1] = w1
		e.Cfg.PlayOff = append(e.Cfg.PlayOff, log)
	}

	w1, l1 = e.PlayBO5(&e.Cfg.PlayOff[11])
	e.QueryTeam(l1).Level += e.Cfg.SLevel[1]
	e.QueryTeam(w1).Level += e.Cfg.SLevel[0]

	sort.Slice(e.Cfg.Teams, func(i, j int) bool {
		return e.Cfg.Teams[i].Level > e.Cfg.Teams[j].Level
	})
}

func (e *Elo) Ticks() {
	e.Worlds[0] = e.Cfg.Teams[0].Name
	e.Worlds[1] = e.Cfg.Teams[1].Name

	var t0 string
	var t1 string
	var log GameLog

	if 12 == len(e.Cfg.PlayOff) {
		log.Teams[0] = e.Cfg.Teams[2].Name
		log.Teams[1] = e.Cfg.Teams[3].Name
		e.Cfg.PlayOff = append(e.Cfg.PlayOff, log)
	}
	e.Worlds[2], t0 = e.PlayBO5(&e.Cfg.PlayOff[12])

	if 13 == len(e.Cfg.PlayOff) {
		log.Teams[0] = e.Cfg.Teams[4].Name
		log.Teams[1] = e.Cfg.Teams[5].Name
		e.Cfg.PlayOff = append(e.Cfg.PlayOff, log)
	}
	t1, _ = e.PlayBO5(&e.Cfg.PlayOff[13])

	if 14 == len(e.Cfg.PlayOff) {
		log.Teams[0] = t0
		log.Teams[1] = t1
		e.Cfg.PlayOff = append(e.Cfg.PlayOff, log)
	}
	e.Worlds[3], _ = e.PlayBO5(&e.Cfg.PlayOff[14])
}
