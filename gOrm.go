package main

import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

type LplElo struct {
	gorm.Model
	Idx  int
	Team LPLTeam `gorm:"embedded;"`
}

var g_mailDB *gorm.DB

func OrmInit() error {
	var err error
	dsn := "miao3rd:ready2DIE@tcp(rm-wz9j65qsi2ga55ta1.mysql.rds.aliyuncs.com:3306)/lpl?charset=utf8mb4&parseTime=True"
	g_mailDB, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		return err
	}
	err = g_mailDB.Set("gorm:table_options", "ENGINE=MyISAM").AutoMigrate(&LplElo{})
	if err != nil {
		return err
	}

	return nil
}
