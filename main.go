package main

import (
	"flag"
	"fmt"
)

var typeFlag = flag.String("type", "", "do type [\"gorm\", \"img\", \"lpl\"]")
var fileFlag = flag.String("file", "", "img filename")

func main() {
	flag.Parse()

	var err error
	switch *typeFlag {
	case "img":
		err = DoImgSplit(*fileFlag)
	case "lpl":
		err = OrmInit()
		if err == nil {
			err = DoLPL(*fileFlag)
		}
	case "skill":
		err = DoSkillCalc(*fileFlag)
	}

	if err != nil {
		fmt.Println(err.Error())
	} else {
		fmt.Println("done")
	}
}
